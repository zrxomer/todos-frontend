// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAkC0mgRwvHeH53FLEN3R05N7eASOcApKQ",
    authDomain: "pushnotification-c9f91.firebaseapp.com",
    projectId: "pushnotification-c9f91",
    storageBucket: "pushnotification-c9f91.appspot.com",
    messagingSenderId: "449649620001",
    appId: "1:449649620001:web:ed615d85e26e6e6b8fdf60",
    measurementId: "G-4J6P45N0GC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
