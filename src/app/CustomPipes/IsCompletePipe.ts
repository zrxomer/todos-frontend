import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "IsCompletePipe"
})
export class IsCompletePipe implements PipeTransform {
  transform(value: boolean): string {
    if (value) {
      return '<span class="badge badge-success">true</span>';
    }
    return '<span class="badge badge-danger">false</span>';
  }


}
