import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {LocalStorageServiceService} from './local-storage-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: LocalStorageServiceService,
              private router: Router,
  ) {
  }

  canActivate(): boolean {
    if (this.auth.isLoggedIn) {
      return true;
    } else
      this.router.navigate(['/signin'])
    return false;
  }
}
