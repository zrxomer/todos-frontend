import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpHandlerInterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const userToken=localStorage.getItem('Token');
    console.log(userToken);
    const modifiedRequest =request.clone({
     // headers: request.headers.set('Authorization',`Bearer ${userToken}`)
      headers:request.headers.set('Authorization', 'Bearer ' + userToken)
    })

    return next.handle(modifiedRequest);
  }
}
