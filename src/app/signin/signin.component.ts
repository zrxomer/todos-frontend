import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {UserServiceService} from "../user-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  userPasswordControl: FormControl;
  userEmailControl: FormControl;
  fetchedData: any;
  PassWordValidator: string = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)\\S{6,20}$";
  ErrorCode: string;
  UserRole: any;
  token: string;
  ErrorCodeRole: string;
  LoginStatus: string;
  SubjectUserLogin: string = "SignIn_Subject";

  constructor(
    private router: Router,
    private userServices: UserServiceService) {
    this.userServices.SubjectUserLogin.subscribe(res => {
      this.SubjectUserLogin = res;
    });
    this.userEmailControl = new FormControl('', [Validators.required, Validators.email]);
    this.userPasswordControl = new FormControl('', [Validators.required, Validators.pattern(this.PassWordValidator)]);
    this.fetchedData = "";
    this.ErrorCode = "";
    this.token = "";
    this.ErrorCodeRole = "";
    this.LoginStatus = "";
  }

  setTestData() {
    this.userEmailControl.setValue('admin@gmail.com');
    this.userPasswordControl.setValue('Admin@123');
  }

  ngOnInit(): void {
    this.setTestData();
  }

  LoginUser() {
    this.userServices.checkUserLogin({
      email: this.userEmailControl.value,
      password: this.userPasswordControl.value,
    }).subscribe(result => {
      this.fetchedData = result;
      localStorage.setItem('Token', this.fetchedData.data);
      if (this.fetchedData != null) {
        this.token = this.fetchedData.data;

        //Logic to get admin or user from backend
        this.userServices.getUserRole(this.token).subscribe(re => {
          this.UserRole = re;
          if (this.UserRole.role == "Admin" || this.UserRole.role == "User") {
            if (this.UserRole.role == "Admin") {
              this.LoginStatus = "Admin Logged in";
              this.userServices.passUserRole(this.UserRole.role);
              this.userServices.passEmail(this.userEmailControl.value)
              this.router.navigateByUrl('/admin')
              this.userServices.SubjectUserLogin.next(this.UserRole.role)
            } else {
              this.LoginStatus = "User Logged in";
              this.userServices.passUserRole(this.UserRole.role);
              this.userServices.passEmail(this.userEmailControl.value)
              this.router.navigateByUrl('/todos')
            }

          } else {
            this.LoginStatus = "Non authorized User, Please register first.";
          }
        }, error => {
          this.ErrorCodeRole = error.status;
        })


      }
    }, (err) => {
      this.ErrorCode = err.status;
    });
  }

}
