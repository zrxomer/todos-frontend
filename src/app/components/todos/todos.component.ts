import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserServiceService} from 'src/app/user-service.service';
import {Todo} from '../../models/Todo';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todoIdControl: FormControl;
  todoControl: FormControl;
  todoIsCompleteIdControl: FormControl;
  todoIsCompleteStatus: FormControl;
  IsCompleteBool: boolean;
  userEmail:string;
  UserTodo:string;
  closeResult = '';
  ModalTodo: string="";

  TotalRecords:any;
  PageNumber:number=1;

  popuptodos= new FormGroup({
    Usertodo: new FormControl('')
  })

  SubjectUserLogin:string="Todos_Subject";

  constructor(private todoService: UserServiceService ,private modalService: NgbModal) {
    this.todoService.SubjectUserLogin.subscribe(res=>{
      this.SubjectUserLogin=res;
    })
    this.todoControl = new FormControl('', [Validators.required])
    this.todoIdControl = new FormControl(0, [Validators.required, Validators.min(1)])
    this.todoIsCompleteIdControl = new FormControl(1, [Validators.required, Validators.min(1)])
    this.todoIsCompleteStatus = new FormControl()
    this.IsCompleteBool = false;
    this.userEmail="";
    this.UserTodo="";
    this.TotalRecords="";
  }


  ngOnInit(): void {

    this.recvEmail();
    this.getUserTodos();
  }

  ufdata: Todo[] = [];


  recvEmail(){
    this.userEmail = this.todoService.receiveEmail();
  }
  addtodo() {
    this.ModalTodo=this.popuptodos.controls['Usertodo'].value;
    let TodoPrompt = this.ModalTodo;
    let userNamePrompt = this.userEmail;
    if (TodoPrompt == null) {
      return;
    }
    if (userNamePrompt == null) {
      return;
    }

    this.todoService.postData({
      isComplete: false,
      userName: userNamePrompt,
      todoName: TodoPrompt,
      id: 0
    }).subscribe(result => {
      this.todoControl.reset();
      this.getUserTodos();

    });
  }

  deletetodo(id: number) {
    this.todoService.deleteData(id).subscribe(result => {
      this.getUserTodos();
    });
  }

  UpdateTodo() {
    let TodoAlert = prompt("Please enter your Todo");
    let userNamePrompt = this.userEmail;
    if (userNamePrompt == null) {
      return;
    }
    if (TodoAlert == null) {
      return;
    }
    this.todoService.UpdateData(+this.todoIdControl.value, {
      isComplete: false,
      userName: userNamePrompt,
      todoName: TodoAlert,
      id: 0
    }).subscribe(result => {
      this.todoControl.reset();
      this.getUserTodos();
    });
  }

  UpdateIsCompleteUsingId(id: number) {
    let TodoStatus = prompt("Is Todo Completed? (true/false)")
    if (TodoStatus == null) {
      return
    }
    if (TodoStatus == 'true') {
      this.IsCompleteBool = true;
    } else {
      this.IsCompleteBool = false;
    }
    this.todoService.UpdateIsCompleteById(id, {
      isComplete: this.IsCompleteBool,
      userName: "Umer",
      todoName: 'ff',
      id: 0
    }).subscribe(result => {
      this.todoControl.reset();
      this.getUserTodos();
    });
  }

  UpdateUsingJs(id: number) {

    let TodoAlert = prompt("Please enter your Todo..");
    let userNamePrompt = this.userEmail;

    if (userNamePrompt == null) {
      return;
    }
    if (TodoAlert == null) {
      return;
    }
    this.todoService.UpdateData(+id, {
      isComplete: false,
      userName: userNamePrompt,
      todoName: TodoAlert,
      id: 0
    }).subscribe(result => {
      this.todoControl.reset();
      this.getUserTodos();
    });
  }
  getUserTodos(){
    this.todoService.getUserTodos({
      id: 0,
      userName:this.userEmail,
      todoName: "test",
      isComplete:false,
    }).subscribe(result=>{
      this.ufdata=result as Todo[];
      this.TotalRecords=this.ufdata.length;
    })

  }




  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
