import {Component} from '@angular/core';
import {Todo} from './models/Todo';
import {UserServiceService} from './user-service.service'
import {LocalStorageServiceService} from "./local-storage-service.service";
import {Router} from "@angular/router";
import { MessagingService } from './messaging.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  AdminUser:string;
  message:any;


  constructor(private router:Router,private user: UserServiceService,
              private localStorageService: LocalStorageServiceService,
              private messagingService: MessagingService) {

    this.AdminUser="";
  }
  ngOnInit() {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;

  }


  get isLoggedIn(): boolean {
    this.AdminUser=this.user.receiveUserRole();
    return this.localStorageService.isLoggedIn;
  }

  logOut() {
    this.localStorageService.clear();
    this.router.navigateByUrl('/signin')


  }


}
