import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TodosComponent } from './components/todos/todos.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SigninComponent } from './signin/signin.component';
import { RegisterComponent } from './register/register.component'
import {IsCompletePipe} from "./CustomPipes/IsCompletePipe";
import {NgxPaginationModule} from "ngx-pagination";
import { AdminComponent } from './admin/admin.component';
import {LocalStorageServiceService} from "./local-storage-service.service";
import {HttpHandlerInterceptorInterceptor} from "./http-handler-interceptor.interceptor";
import {ErrorCheckingInterceptor} from "./error-checking.interceptor";

import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { MessagingService } from './messaging.service';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    SigninComponent,
    RegisterComponent,
    IsCompletePipe,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    NgxPaginationModule,
    AppRoutingModule,
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers:[[LocalStorageServiceService],
            {provide: HTTP_INTERCEPTORS,useClass:HttpHandlerInterceptorInterceptor,multi:true},
            {provide: HTTP_INTERCEPTORS,useClass:ErrorCheckingInterceptor,multi:true},
            MessagingService,AsyncPipe] ,



  bootstrap: [AppComponent]
})
export class AppModule { }
