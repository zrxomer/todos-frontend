import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserServiceService} from "../user-service.service";
import {Subscription} from "rxjs";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {

  SubjectUserLogin: string = "Admin_Subject";
  subscriptions: Subscription[] = [];
  users: any = [];
  fetchedTodos: any = [];
  showUserData: boolean = false;
  showAdminData: boolean = false;
  userEmail: string;
  userPassword: string;
  closeResult: any;
  getDismissReason: any;
  sortingParameter:string="username";



  private _totalPages: number = 0;
  get totalPages() {
    return this._totalPages;
  }
  set totalPages(value: number) {
    this._totalPages = value;
    this.pages = Array(this.totalPages).fill(0).map((x, i) => i);
  }
  addUsersForm = new FormGroup({
    userEmail: new FormControl(''),
    userPassword: new FormControl('')
  })

  delUserForm = new FormGroup({
    delUserEmail: new FormControl('')
  })
  pages: number[] = [];

  constructor(private _UserService: UserServiceService, private modalService: NgbModal) {
    this.subscriptions.push(
      this._UserService.SubjectUserLogin.subscribe(res => {
        this.SubjectUserLogin = res;
      }));
    this.userEmail = "";
    this.userPassword = "";
    this.totalPages = 0;
  }


  ngOnInit(): void {
    this._UserService.getTotalPages().subscribe(res => {
      this.totalPages = res as number;
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(p => {
      p.unsubscribe();
    });
  }


  getUsers() {
    this.showUserData = true;
    this._UserService.getUserDataForAdmin().subscribe(result => {
      this.users = result;
    })
  }

  addUser() {
    console.log('I am here');
    this._UserService.addUserDataForAdmin({
      email: this.addUsersForm.controls['userEmail'].value,
      password: this.addUsersForm.controls['userPassword'].value
    }).subscribe(result => {
      this.getUsers();
    });
  }

  deleteUser() {

    let email = this.delUserForm.controls['delUserEmail'].value;
    if (email == null) {
      return;
    }
    console.log(email);
    this._UserService.deleteUserForAdmin(email).subscribe();
    this.getUsers();
  }


  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  getTodos() {
    this.showUserData = false;
    this.showAdminData = true;
    this._UserService.getTodosForAdmin().subscribe(res => {
      this.fetchedTodos = res;
    });
  }

  setPages() {
    if (this.totalPages > 0) {

    }
  }


  getUserDataByPaging(pageNumber:number){
    this._UserService.getDataByPaging().subscribe(res=>{
      pg:pageNumber;
      sort:this.sortingParameter;

    });



  }
}
