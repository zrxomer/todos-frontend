export class Todo {
  id: number = 0;
  userName: string = "";
  todoName: string = "";
  isComplete: boolean = false;
}
