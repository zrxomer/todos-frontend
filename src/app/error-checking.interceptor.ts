import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

import { retry, catchError } from 'rxjs/operators';
@Injectable()
export class ErrorCheckingInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return next.handle(request).
    pipe(
      // Retry on failure
      retry(0),

      // Handle errors
      catchError((error: HttpErrorResponse) => {
        // error handling logic here

        let errorMessage = '';
        console.log(error.error.data)

        if (error.error instanceof ErrorEvent) {

          // client-side error


          errorMessage = `ClientSideError: ${error.error.message}`;

        } else {

          // server-side error

          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;

        }
        window.alert(errorMessage);


        return throwError(error);
      })
    );

  }
}
