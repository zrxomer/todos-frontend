import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {UserServiceService} from "../user-service.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  userEmailControl:FormControl;
  userPasswordControl:FormControl;
  ErrorCode:string;
  PassWordValidator:string="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)\\S{6,20}$";



  constructor(private UserService:UserServiceService) {
    this.userEmailControl = new FormControl('',
      [Validators.required, Validators.email]);
    this.userPasswordControl = new FormControl('',[Validators.required,Validators.pattern(this.PassWordValidator)]);
    this.ErrorCode='';
  }

  ngOnInit(): void {
  }

  RegisterUser() {
    this.UserService.postUserData({
      email:this.userEmailControl.value,
      password:this.userPasswordControl.value,
    }).subscribe(result => {
      //this.userEmailControl.reset();
      //this.userPasswordControl.reset();
      alert("Congrats! You are registered");
    },(err) => {
      this.ErrorCode=err.status;
    });
  }
}
