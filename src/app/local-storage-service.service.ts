import {Injectable} from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
   providedIn: "root"
})
export class LocalStorageServiceService {

  private _token = "Token";
  get isLoggedIn(): boolean {
    return localStorage.getItem(this._token) != null;
  }
  setToken(token: string): void {
    localStorage.setItem(this._token, token);
  }

  getToken(token: string): string | null {
    return localStorage.getItem(this._token);
  }
  clear() {
    localStorage.clear();
  }
}
