import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import {Validators} from '@angular/forms';
import {Todo} from './models/Todo';
import {User} from './models/user'
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  SubjectUserLogin = new Subject<any>();
  /*
    getHeaders(token: string) {
      let header: HttpHeaders = new HttpHeaders();
      header = header.append('Authorization', 'Bearer ' + token);
      return header;
    }
  */
  userEmailPassing: string;
  UserRole: string;

  constructor(private http: HttpClient) {
    this.userEmailPassing = "";
    this.UserRole = "";
  }

  getData() {
    let url = "https://localhost:5001/api/Todo/";
    return this.http.get(url);
  }

  postData(todo: Todo) {
    let url = "https://localhost:5001/api/Todo/";
    return this.http.post(url, todo);
  }

  deleteData(id: number) {
    let url = "https://localhost:5001/api/Todo/" + id;
    return this.http.delete(url);
  }

  UpdateData(id: number, todo: Todo) {
    let url = "https://localhost:5001/api/Todo/" + id;
    return this.http.put(url, todo);
  }

  UpdateIsCompleteById(id: number, todo: Todo) {
    let url = "https://localhost:5001/api/Todo/updateiscomplete/" + id;
    return this.http.put(url, todo);
  }

  postUserData(user: User) {
    let url = "https://localhost:5001/api/Users";
    return this.http.post(url, user);
  }

  checkUserLogin(user: User) {
    let url = "https://localhost:5001/api/Users/gettoken";
    return this.http.post(url, user);
  }

  getUserTodos(todo: Todo) {
    let url = "https://localhost:5001/api/Todo/todosbyuser";
    return this.http.put(url, todo);
  }


  getUserRole(token: string) {
    let url = "https://localhost:5001/api/Users/validatetoken";
    return this.http.get(url);
    //,{headers: this.getHeaders(token)}

  }

  //For Passing Values Between Components(User Email)
  passEmail(data: string) {
    this.userEmailPassing = data;
  }

  receiveEmail() {
    return this.userEmailPassing;
  }

  passUserRole(role: string) {
    this.UserRole = role;
  }

  receiveUserRole() {
    return this.UserRole;
  }

  ///////////////////////////////////////////////
  //Admin Functionalities

  getUserDataForAdmin() {
    let url = "https://localhost:5001/api/Admin/";
    return this.http.get(url);
  }

  addUserDataForAdmin(user: User) {
    let url = "https://localhost:5001/api/Admin/";
    return this.http.post(url, user);
  }

  deleteUserForAdmin(email: String) {
    let url = "https://localhost:5001/api/Admin/" + email;
    return this.http.delete(url);

  }

  getTodosForAdmin() {
    let url = "https://localhost:5001/api/Admin/AllTodos";
    return this.http.get(url);
  }


  //Server side pagination


  getTotalPages() {
    let url = "https://localhost:5001/api/Todo/tpages";
    return this.http.get(url);
  }

  getDataByPaging(page: number, sort: string) {
    let url = "https://localhost:5001/api/Todo/stodos";
    const httpParams = new HttpParams()
      .set('page', page.toString())
      .set('sort', sort);
    return this.http.get(url, {params: httpParams});

  }


}

