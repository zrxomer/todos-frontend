import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterComponent} from './register/register.component';
import {SigninComponent} from './signin/signin.component';
import {TodosComponent} from './components/todos/todos.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./auth.guard";
import {AdminComponent} from "./admin/admin.component";

const routes: Routes = [

  {
    path: 'todos', component: TodosComponent,
    canActivate: [AuthGuard]
  },
  {path: 'signin', component: SigninComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: 'admin', component: AdminComponent,
    canActivate: [AuthGuard]
  }/*,
  {
    path:'**',
    redirectTo: 'signin'
  }*/
];


@NgModule({
  declarations: [],
  imports: [[RouterModule.forRoot(routes)],
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
